# Python development challenge - Enron Emails

Please complete the following data processing challenge by Sunday, 26th January 2020 (latest at 23:59 GMT+2).


### Description

Download the May 7, 2015 Enron email dataset from https://www.cs.cmu.edu/~./enron/.

The challenge is to create a program that parses the Enron email dataset and answers questions outlined below.

The dataset is rather big (1.82Gb gzipped), so don't be discouraged if that doesn't necessarily fit so nicely in memory at once. The trick here is to deal with the size  for example by reducing the size based on the task, or dealing only with a part of the data in memory at once.

### Task outline

#### 1) Calculate how many emails were sent from each sender address to each recipient.

The result should be a CSV file that contains three columns (with header row included):  

- sender: the sending email address,
- recipient: the recipient email address
- count: number of emails sent from sender to recipient

If an email has multiple recipients, CC's or BCC's, count the email as it would have been sent to each recipient individually.

To simplify the task, you can consider "sent emails" as those that reside in the sent folders (sent / sent items).

#### 2) Calculate the average number of emails received per day per employee per day of week (monday, tuesday, etc.).

An employee is here defined as a person whos shortened name appears on the folder names on maildir, for example taylor-m. For simplicity, you can consider only emails residing in the inbox folder of each respective employee folder as received emails.

The result should be a CSV file that contains three columns (with header row included):  

- employee: the shortname of the employee
- day_of_week: day of week is a number 0-6, where 0 is monday, 1 tuesday etc
- avg_count: average number of emails received on the corresponding day of week by the corresponding employee


### Submission Requirements

- All submissions must be in python (version 3.7)
- Use pipenv to package and version the dependencies and include Pipfile and Pipfile.lock in version control (https://github.com/pypa/pipenv)
- Use version control (bitbucket or github) throughout the challenge (displaying the progress of your work)
- The submission should include a python program to process and output the two required csv files
- The submission must contain a README.md file with instructions how to run the solution (along with its dependencies), using the pipenv defined in the repository.

The solution program should output exactly two csv files, named:  

- emails_sent_totals.csv
- emails_sent_average_per_weekday.csv

Make your repository public and submit your complete solution as a link to the public repository via email.


### Bonus points

In order to maximize reader happiness, please follow Clean Code principles:

For example, check https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29
